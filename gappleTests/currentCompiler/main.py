import os
import sys
import time
import subprocess
import signal
os.system("rm get-pip.py")
os.system("wget https://bootstrap.pypa.io/get-pip.py")
os.system("python3 get-pip.py")
os.system("pip install termcolor")
#os.system("pip install oauth2client")
#os.system("pip install easy-google-docs")
#from easygoogledocs import GoogleAPI, AUTH_TYPE_BROWSER, AUTH_TYPE_SERVICE_ACCOUNT
#api = GoogleAPI(credentials_file='credentials.json')
#api.authorize(authentication_type=AUTH_TYPE_BROWSER)
from termcolor import colored
os.system("clear")
##printing the logo
print("MMMMMMMMMMMMMMWWWWWWWWMMMMMMMMMMMMMMMMMMRA")
print(("MMMMMMMMMMMMMMNk:"), colored(".....;", "yellow"), ("xXMMMMMMMMMMMMMNY"))
print(("MMMMMMMMMMMWNNWMX;"), colored("..''..", "yellow"), ("kMWWNNWMMMMMMML;"))
print(("MMMMMMNK00Oo:cx0x"), colored("' .''..", "yellow"), ("l0ko:lx000XMMMRX"))
print(("MMMMW0o:"), colored(";;:ldoc;,", "green"), colored("...''. ", "yellow"), colored("ldoc;;;", "green"), ("cxXMX;R"))
print(("MMMKo"), colored(":cdxxxxxxx", "green"), colored(",...'", "yellow"), colored(":oxxxkkkkxxo:", "green"), (":kWML"))
print(("MMMO'"), colored(";xxxxxxxxxxxxdolloxxxxxOKX0kxxl", "green"), (".l;M"))
print(("MW0l:"), colored("lxxxxxxxxxxxxxxxxxxxxxxkk0Okxxd:", "green"), (":;L"))
print(("MNl."), colored("lxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx,", "green"), ("XM;"))
print(("MNl."), colored("lxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", "green"), (";OM"))
print(("MNl."), colored("lxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", "green"), ("Olx"))
print(("MWl'"), colored("lkxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", "green"), (";xl"))
print(("MWl'"), colored("lxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", "green"), (";lx"))
print(("MWl'"), colored("lxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxd", "green"), (";OX"))
print(("MWXx;"), colored("cxxxxxxxxxxxxxxxxxxxxxxxxxxxxxo;", "green"), ("c:O"))
print(("MMMO,:"), colored("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxl'", "green"), ("o;x"))
print(("MMMNkc:"), colored("dxxxxxxxxxxxxxxxxxxxxxxxxxxl", "green"), ("M;MXA"))
print(("MMMMWx:"), colored("cdxxxxxxxxxxxxxxxxxxxxxxxxo:", "green"), ("lKMMM"))
print(("MMMMMWKdc:"), colored("coxxxxxxoc::ldxxxxxdc:", "green"), ("clONWMMM"))
print("MMMMMMMWKkxo::::::lxkkdc:c:::cdkONMMMMMMRX")
print("MMMMMMMMMMWX000O00XWMMN0000000NMMMMMMMMMOD")
##end of printing the logo
def main5(properties) :
    targetFile = open(("./input/"+properties), "r")
    mainFile = open("./output/output.py", "w")
    mainFile.truncate(0)
    currentLine = targetFile.readline()
    currentLine = currentLine.lower()
    if "gui" in currentLine:
      appType = "gui"
      mainFile.write("# -*- coding: utf-8 -*-\n")
      mainFile.write("import os\n")
      mainFile.write("from tkinter import *\n")
      mainFile.write("from Tkinter import *\n")
      mainFile.write("mainScreen = Tk()\n")
    elif "cli" in currentLine:
      appType = "cli"
      mainFile.write("# -*- coding: utf-8 -*-\n")
      mainFile.write("import os\n")
      mainFile.write("import platform\n")
      mainFile.write("if platform.system() == 'Darwin':\n")
      mainFile.write("  currentOS = 'MacOS'\n")
      mainFile.write("elif platform.system() == 'Windows':\n")
      mainFile.write("  currentOS = 'Windows'\n")
      mainFile.write("elif platform.system() == 'Linux':\n")
      mainFile.write("  currentOS = 'Linux'\n")
    else:
      print(colored("Error! The application type has not been defined in the first line! Please do this as appType = cli or appType = gui!", "red"))
    print(currentLine)
    while currentLine != "":
      currentLine = targetFile.readline()
      if "printIn" in currentLine:
        #alternative result :P 
        #quotationMarkPos = currentLine.find('(')
        #quotationMark2Pos = currentLine.find(')')
        #textToPrintIn = currentLine[(quotationMarkPos + 1):(quotationMark2Pos)]
        if appType == "cli":
          result = currentLine.replace("printIn", "print")
          mainFile.write(result + "\n")
          #if "  " in currentLine:
            #startPos = currentLine.find("computer.printIn")
            #mainFile.write(currentLine[0:(startPos)])
            #mainFile.write("print("+textToPrintIn+")\n")
          #else:
            #mainFile.write("print("+textToPrintIn+")\n")
        else:
          if "  " in currentLine:
            startPos = currentLine.find("printIn")
            mainFile.write(currentLine[0:(startPos)])
            mainFile.write('printedInText = Label(mainScreen, text='+ textToPrintIn +')\n')
            #mainFile.write("printedInText.pack()")
          else:
            mainFile.write('printedInText = Label(mainScreen, text='+ textToPrintIn +')\n')
            mainFile.write("printedInText.pack()")
      elif "getInput" in currentLine:
        quotationMarkPos = currentLine.find('(')
        quotationMark2Pos = currentLine.find(',')
        hintForAsk = currentLine[(quotationMarkPos + 1):(quotationMark2Pos)]
        if "multi-line" in currentLine:
          if "  " in currentLine:
            uPos = currentLine.find("getInput")
            mainFile.write(currentLine[(0):(uPos)])
            mainFile.write("lastInput = []\n")
            mainFile.write(currentLine[(0):(uPos)])
            mainFile.write("lastInputLast = input("+hintForAsk+")\n")
            mainFile.write(currentLine[(0):(uPos)])
            mainFile.write("lastInput.append(lastInputLast)\n")
            mainFile.write(currentLine[(0):(uPos)])
            mainFile.write("lastInputLast = input('')\n")
            mainFile.write(currentLine[(0):(uPos)])
            mainFile.write("lastInput.append(lastInputLast)\n")
            mainFile.write(currentLine[(0):(uPos)])
            mainFile.write("while lastInputLast != '':\n")
            mainFile.write(currentLine[(0):(uPos)])
            mainFile.write("  lastInputLast = input('')\n")
            mainFile.write(currentLine[(0):(uPos)])
            mainFile.write("  lastInput.append(lastInputLast)\n")
          else:
            mainFile.write("lastInput = []\n")
            mainFile.write("lastInputLast = input("+hintForAsk+")\n")
            mainFile.write("lastInput.append(lastInputLast)\n")
            mainFile.write("lastInputLast = input('')\n")
            mainFile.write("lastInput.append(lastInputLast)\n")
            mainFile.write("while lastInputLast != '':\n")
            mainFile.write("  lastInputLast = input('')\n")
            mainFile.write("  lastInput.append(lastInputLast)\n")
        elif "single-line" in currentLine:
          if "  " in currentLine:
            uPos = currentLine.find("getInput")
            mainFile.write(currentLine[(0):(uPos)])
            mainFile.write("lastInput = input("+hintForAsk+")\n")
          else:
            mainFile.write("lastInput = input("+hintForAsk+")\n")
        else:
          print(colored('Error! Please tell me the input should be multi-line or single-line! Eg: user.getInput("text before the input", multi-line).', "red"))
      elif "if" in currentLine:
        mainFile.write(currentLine+"\n")
      elif "else" in currentLine:
        mainFile.write(currentLine+"\n")
      elif "elif" in currentLine:
        mainFile.write(currentLine+"\n")
      elif "shortcut" in currentLine:
        mainFile.write(currentLine.replace("shortcut", "def"))
      elif "forever" in currentLine:
        if "  " in currentLine:
          mainFile.write(currentLine[currentLine.find("forever") : (0)])
        mainFile.write("while True:")
      elif "getVoiceInput" in currentLine:
        languageStart = currentLine.find("(")
        languageStart = (languageStart + 1)
        languageEnd = currentLine.find(")")
        nonEmptyLineStart = currentLine.find("getVoiceInput")
        currentLanguage = currentLine[languageStart:languageEnd]
        if "  " in currentLine:
          tabSection = currentLine[0:nonEmptyLineStart]
        else:
          tabSection = ""
        mainFile.write(tabSection + 'os.system("pip3 install SpeechRecognition")\n')
        mainFile.write(tabSection + 'import speech_recognition as sr\n')
        mainFile.write(tabSection + "r = sr.Recognizer()\n")
        mainFile.write(tabSection + "with sr.Microphone() as source:\n")
        mainFile.write(tabSection + "  audio = r.listen(source)\n")
        mainFile.write(tabSection + "  lastInput = r.recognize_google(audio, language=" + currentLanguage + ")\n")
      elif "usePackage" in currentLine:
        packageNameStart = currentLine.find("(")
        packageNameEnd = currentLine.find(")")
        packageNameEnd = (packageNameEnd + 1)
        packageToUse = currentLine[packageNameStart:packageNameEnd]
        mainFile.write('os.system("git clone https://piciakk@bitbucket.org/piciakk/greenapple.git")\n')
        mainFile.write("packageToUse = " + packageToUse + "\n")
        mainFile.write('currentWorkingDirectory = os.popen("pwd")\n')
        mainFile.write('if currentOS == "MacOS" or currentOS == "Linux":\n')
        mainFile.write('  os.system("python3 " + currentWorkingDirectory.read() + "/greenapple/packages/" + packageToUse + "/install.gapple")\n')
        mainFile.write('elif currentOS == "Windows":\n')
        mainFile.write('  wgetAppCode = os.popen("curl https://gappleproject.piciakk.repl.co/wget.exe")\n')
        mainFile.write('  wgetAppCode = wgetAppCode.read()\n')
        mainFile.write('  wgetAppPath = (currentWorkingDirectory.read() + "/wget.exe")\n')
        mainFile.write('  os.system("touch " + wgetAppPath)\n')
        mainFile.write('  wgetApp = open(wgetAppPath)\n')
        mainFile.write('  wgetApp.write(wgetAppCode)\n')
        mainFile.write('  os.system(wgetAppPath + " https://gappleproject.piciakk.repl.co/pythonFiles/python.exe")\n')
        mainFile.write('  os.system(wgetAppPath + " https://gappleproject.piciakk.repl.co/pythonFiles/python37.dll")\n')
        mainFile.write('  pythonApp = (currentWorkingDirectory.read() + "/python.exe")\n')
        mainFile.write('  os.system(pythonApp + currentWorkingDirectory.read() + "/greenapple/packages/" + packageToUse + "/install.gapple")\n')
        #mainFile.write(packageToUse + ' = open(currentWorkingDirectory.read() + "/greenapple/packages/' + packageToUse + '/shortcuts.gapple", "r")\n')
      else:
        mainFile.write(currentLine)
      print(currentLine)
def main4(mode, properties) :
  if mode == "fullcode":
    writedFile = open("./input/cmd.gapple", "w")
    writedFile.truncate(0)
    writedFile.write(properties)
    writedFile.close()
    main5("cmd.gapple")
  elif mode == "path":
    main5(properties)
def main2(mode) :
  if mode == "fullcode":
    print("Please insert the full code here!")
    fullCode = input("")
    fullCodeLast = input("")
    while fullCodeLast != "":
      fullCode = (fullCode + "\n" + input(""))
    main4("fullcode", fullCode)
  elif mode == "path":
    print(("Please paste here the filename of your raw code ("), colored("only .gapple files, only in input folder!", "green"), (")"))
    path = input("-")
    main4("path", path)
  else:
    exit
def main() :
  print("Choose how you want to browse your program!")
  print(colored("1.", "green"), ("I'll copy the full code"))
  print(colored("2.", "green"), ("I'll give you the filename of my code"))
  print(colored("3.", "green"), ("EXIT"))
  browseMode = input("-")
  if "1" in browseMode:
    main3("fullcode")
  elif "2" in browseMode:
    main2("path")
print("")
print(colored("Green Apple v1.0", "green"))
main()